import React, { useState, useEffect } from 'react';
import './App.css';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Dropdown } from 'react-bootstrap';
import GGLogo from './images/gigger-logo.svg';
import SearchIcon from './icons/search.svg';

const axios = require('axios');


function search() {
    const searchText = document.querySelector("#searchInput").value;
    if (searchText.length > 0)
        window.location.href = `/search-portfolio-results/?searchText=${searchText}`;
}

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
    </a>
  ));

function App() {
    const [frontUserData, setFrontUserData] = useState({});
    const [workNotify, setWorkNotify] = useState({ client_notify: 0, freelance_notify: 0 });
    const url = process.env.REACT_APP_API_URL || 'http://localhost:8080';

    const signOut = () => {
        axios.post(`${url}/api/sign-out`, {}, { withCredentials: true })
        .then(response => {
            window.location.href = `${url}/home`;
        }).catch(({ response }) => {
            console.log('signOut error', response);
        });
    }

    const switchRole = () => {
        let targetClientLevel = parseInt(frontUserData.client_level) === 1 ? 0 : 1;
        axios.post(`${url}/api/client-level/${targetClientLevel}`, {}, { withCredentials: true })
        .then(response => {
            setFrontUserData(response.data.frontUserData);
        }).catch(({ response }) => {
            console.log('switchRole error', response);
        });
    }

    const testRest = () => {
        (async function() {
            try {
                const response = await axios.get(`${url}/api`, { withCredentials: true });
                console.log('nav (testRest data):', response.data);
            } catch(error) {
                console.log('nav (testRest try error):', error);
            }
        }());
    }

    useEffect(() => {
        const controller1 = new AbortController(), controller2 = new AbortController(), controller3 = new AbortController();
        (async function() {
            try {
                //const response = await axios.get(`${url}/secret-login/8`, { signal: controller1.signal }); //for test
                const response = await axios.get(`${url}/api`, { withCredentials: true, signal: controller1.signal });
                //console.log('nav (useEffect data):', response.data);
                setFrontUserData(response.data.frontUserData);
                if (response.data.frontUserData) {
                    const workres = await axios.get(`${url}/api/work-notify`, { withCredentials: true, signal: controller2.signal });
                    //console.log('nav (work init data):', workres.data);
                    setWorkNotify(workres.data);
                }
            } catch(error) {
                console.log('nav (useEffect try error):', error);
            }
        }());
        const callWorkNotify = setInterval(async function() {
            try {
                const response = await axios.get(`${url}/api/work-notify`, { withCredentials: true, signal: controller3.signal });
                //console.log('nav (setInterval data):', response.data);
                setWorkNotify(response.data);
            } catch(error) {
                console.log('nav (setInterval try error):', error);
            }
        }, 5000);
        return () => {
            controller1.abort();
            controller2.abort();
            controller3.abort();
            clearInterval(callWorkNotify);
        }
    }, [url]);
    
  return (
    <header className="fixed-top">
        <div className="container-fluid">
            <div className="row g-0">
                <div className="col-2 d-flex align-items-center d-lg-none">
                    <a data-bs-toggle="offcanvas" href="#site-sidebar" role="button" aria-controls="site-sidebar">
                        <div className="mobile-menu__button">
                            <div className="mobile-menu__line line--1"></div>
                            <div className="mobile-menu__line line--2"></div>
                            <div className="mobile-menu__line line--3"></div>
                        </div>
                    </a>
                </div>
                <div className="col-8 col-lg-6 d-flex justify-content-center justify-content-lg-start">
                    <div className="d-flex align-items-center me-3">
                        <a href={url + "/home"} target="_top">    
                            <img src={GGLogo} alt="React Logo" width="120px" height="50px"/>
                        </a>
                    </div>
                    <div className="d-none d-lg-flex">
                        <ul className="navbar-nav">
                            <li className="nav-item px-2">
                                <a className="nav-link" href={url + "/home"} target="_top">หน้าหลัก</a>
                            </li>
                            <li className="nav-item px-2">
                                <a className="nav-link" href={url + "/explore-freelance"} target="_top">ฟรีแลนซ์ทั้งหมด</a>
                            </li>
                            <li className="nav-item px-2">
                                <a className="nav-link" href={url + "/explore-jobpost"} target="_top">บอร์ดงาน</a>
                            </li>
                            <li className="nav-item px-2 position-relative">
                                <a className="nav-link" href={url + "/profile/works"} target="_top">งานของฉัน</a>
                                { parseInt(frontUserData.client_level) === 0 && workNotify.freelance_notify ? 
                                    <span className="position-absolute translate-middle badge rounded-pill bg-danger worknumbernotify" style={{right:'-11px',top:'5px'}}>
                                        {workNotify.freelance_notify}
                                        <span className="visually-hidden">primary work notify</span>
                                    </span> : <></>
                                }
                                { parseInt(frontUserData.client_level) !== 0 && workNotify.client_notify ? 
                                    <span className="position-absolute translate-middle badge rounded-pill bg-danger worknumbernotify" style={{right:'-11px',top:'5px'}}>
                                        {workNotify.client_notify}
                                        <span className="visually-hidden">primary work notify</span>
                                    </span> : <></>
                                }
                            </li>
                            <li className="nav-item px-2">
                                <a className="nav-link" href="#" target="_top">คำถามที่พบบ่อย</a>
                            </li>
                            {/*
                            <li className="nav-item px-2">
                                <button className="nav-link" type="button" target="_top" onClick={testRest}>Dev test</button>
                            </li>
                            */}
                        </ul>
                    </div>
                </div>
                <div className="col-2 col-lg-6 d-none d-lg-flex justify-content-end align-items-center">
                {(parseInt(frontUserData.client_level) ===  0) ?
                    <div className="input-group navbar__search toggled nav-freelance pe-3">
                        <div className="input-group-prepend" onClick={search}>
                            <span className="input-group-text" id="basic-addon1">
                                <img alt="" src={SearchIcon} width="20px" height="20px"/>
                            </span>
                        </div>
                        <input type="text" className="form-control btn-rounded" placeholder="ค้นหา Gigger , Type , job..." id="searchInput"/>
                    </div> :
                    <div className="input-group navbar__search pe-3">
                    <div className="input-group-prepend" onClick={search}>
                        <span className="input-group-text" id="basic-addon1">
                            <img alt="" src={SearchIcon} width="20px" height="20px"/>
                        </span>
                    </div>
                    <input type="text" className="form-control btn-rounded" placeholder="ค้นหา Gigger , Type , job..." id="searchInput"/>
                </div>
                }
                    <ul className="navbar-nav">
                        {(parseInt(frontUserData.client_level) ===  0) ? <></> : 
                            <li className="nav-item px-3">
                                <a href={url + "/explore-freelance"} className="btn btn-outline-dark btn-rounded" type="button">จ้างฟรีแลนซ์</a>
                            </li>
                        }
                        <li className="nav-item px-3 position-relative">
                            <a className="nav-link" href={url + "/chat"} target="_top">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512.0002" fill="currentColor" data-src="/icons/chat.svg" width="30px" height="30px" className="lazyloaded injected-svg"><path d="m256 0c-141.484375 0-256 114.496094-256 256 0 44.902344 11.710938 88.757812 33.949219 127.4375l-32.984375 102.429688c-2.300782 7.140624-.410156 14.96875 4.894531 20.273437 5.253906 5.253906 13.0625 7.214844 20.273437 4.894531l102.429688-32.984375c38.679688 22.238281 82.535156 33.949219 127.4375 33.949219 141.484375 0 256-114.496094 256-256 0-141.484375-114.496094-256-256-256zm0 472c-40.558594 0-80.09375-11.316406-114.332031-32.726562-4.925781-3.078126-11.042969-3.910157-16.734375-2.078126l-73.941406 23.8125 23.8125-73.941406c1.804687-5.609375 1.042968-11.734375-2.082032-16.734375-21.40625-34.238281-32.722656-73.773437-32.722656-114.332031 0-119.101562 96.898438-216 216-216s216 96.898438 216 216-96.898438 216-216 216zm25-216c0 13.804688-11.191406 25-25 25s-25-11.195312-25-25c0-13.808594 11.191406-25 25-25s25 11.191406 25 25zm100 0c0 13.804688-11.191406 25-25 25s-25-11.195312-25-25c0-13.808594 11.191406-25 25-25s25 11.191406 25 25zm-200 0c0 13.804688-11.191406 25-25 25-13.804688 0-25-11.195312-25-25 0-13.808594 11.195312-25 25-25 13.808594 0 25 11.191406 25 25zm0 0"></path></svg>
                            </a>
                            { frontUserData.chat_notify && frontUserData.chat_notify.length ?
                            <span className="position-relative top-0 translate-middle badge rounded-pill bg-danger" style={{left:"10px"}}>
                                {frontUserData.chat_notify.length}
                                <span className="visually-hidden">unread messages</span>
                            </span> : <></>
                            }
                        </li>
                    </ul>
                    <Dropdown>
                        <Dropdown.Toggle as={CustomToggle}>
                            <div className="nav-link" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                <img className="user-avatar" alt="user-avatar" src={url + frontUserData.profile_photo} />
                            </div>
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <li>
                                <a className="dropdown-item" href={url + "/profile/dashboard"} target="_top">
                                    <img alt="" src={url + "/icons/dashboard-icon.svg"} width="16px" height="16px" style={{borderRadius:0}} />
                                    &nbsp;หน้าสรุปภาพรวม
                                </a>
                            </li>
                            <li>
                                <a className="dropdown-item" href="https://web.facebook.com/GiggerTH" target="_top">
                                    <img alt="" src={url +"/icons/chat-icon-filled.svg"} width="16px" height="16px" style={{borderRadius:0}} />
                                    &nbsp;ฝ่ายสนับสนุน
                                </a>
                            </li>
                            <li>
                                <a className="dropdown-item" href={url + "/setting"} target="_top">
                                    <img alt="" src={url + "/icons/settings-icon-filled.svg"} width="16px" height="16px" style={{borderRadius:0}} />
                                    &nbsp;การตั้งค่า
                                </a>
                            </li>
                            { parseInt(frontUserData.client_level) === 2 ? <></> :
                            <li className="position-relative">
                                <button type='button' className="dropdown-item" target="_top" onClick={switchRole} style={{cursor:"pointer"}}>
                                    <img alt="" src={url + "/icons/settings-icon-filled.svg"} width="16px" height="16px" style={{borderRadius:0}} />
                                    &nbsp;เปลี่ยนบทบาทเป็น {parseInt(frontUserData.client_level) === 1 ? 'ฟรีแลนซ์' : 'ผู้ว่าจ้าง'}
                                </button>
                                { parseInt(frontUserData.client_level) === 0 && workNotify.client_notify ?
                                    <span className="position-absolute bottom-0 translate-middle badge rounded-pill bg-danger" style={{right:'-5px'}}>
                                        {workNotify.client_notify}
                                        <span className="visually-hidden">secondary work notify</span>
                                    </span> : <></>
                                }
                                { parseInt(frontUserData.client_level) !== 0 && workNotify.freelance_notify ?
                                    <span className="position-absolute bottom-0 translate-middle badge rounded-pill bg-danger" style={{right:'-5px'}}>
                                        {workNotify.freelance_notify}
                                        <span className="visually-hidden">secondary work notify</span>
                                    </span> : <></>
                                }
                            </li>
                            }
                            <li>
                                <hr className="dropdown-divider"/>
                            </li>
                            <li>
                                <button className="dropdown-item" type="button" target="_top" onClick={signOut} style={{cursor:"pointer"}}>
                                    <img alt="" src={url + "/icons/sign-out-icon-filled.svg"} width="16px" height="16px" style={{borderRadius:0}} />
                                    &nbsp;ออกจากระบบ
                                </button>
                            </li>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </div>
    </header>
  );
}

export default App;
